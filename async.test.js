const https = require('https');

function loadStory() {
	return https.get('https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY');
}

(async function() {
  let data = await loadStory();
  console.log("Yey, story successfully loaded!");
  console.dir(data);
}());
