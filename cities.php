<?php

class Graph
{
  protected $graph;
  protected $visited = array();

  public function __construct($graph) {
    $this->graph = $graph;
  }

  public function breadthFirstSearch($origin, $destination) {
    foreach ($this->graph as $vertex => $adj) {
      $this->visited[$vertex] = false;
    }

    $q = new SplQueue();

    $q->enqueue($origin);
    $this->visited[$origin] = true;

    $path = array();
    $path[$origin] = new SplDoublyLinkedList();
    $path[$origin]->setIteratorMode(
      SplDoublyLinkedList::IT_MODE_FIFO|SplDoublyLinkedList::IT_MODE_KEEP
    );

    $path[$origin]->push($origin);

    $found = false;

    while (!$q->isEmpty() && $q->bottom() != $destination) {
      $t = $q->dequeue();

      if (!empty($this->graph[$t])) {
        foreach ($this->graph[$t] as $vertex) {
          if (!$this->visited[$vertex]) {
            $q->enqueue($vertex);
            $this->visited[$vertex] = true;
            $path[$vertex] = clone $path[$t];
            $path[$vertex]->push($vertex);
          }
        }
      }
    }

    if (isset($path[$destination])) {
        return 1;
    }
    else {
        return 0;
    }
  }
}

function connectedCities($n, $g, $originCities, $destinationCities) 
{
    $_graph = array();
    $_list = array();
    $out = array();

    for ($i = 1; $i <= $n; $i++) {
        for ($j = 1; $j <= $n; $j++) {
            if ($i != $j) {
                $gmp = gmp_strval(gmp_gcd($i, $j));
                if ($gmp > $g) {
                    $_list[] = $j;
                }
            }
        }
        $_graph[$i] = $_list;
        $_list = array();
    }

    $_g = new Graph($_graph);

    for ($i = 0; $i < count($originCities); $i++) {
        $out[] = $_g->breadthFirstSearch($originCities[$i], $destinationCities[$i]);
    }
    
    return $out;
}

$ret = connectedCities(6, 1, [1,2,4,6], [3,3,3,4]);
echo implode(' ', $ret);
?>

