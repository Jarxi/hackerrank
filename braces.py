def is_matched(expression):
    print (expression)
    stack = []

    for i, c in enumerate(expression):
        if c == '(' or c == '[' or c == '{':
            stack.append(i)
        elif c == ')' or c == ']' or c == '}':
            if not stack:
                # first breaking close brace
                return False
            else:
                stack.pop()
        else:
            # other char
            continue

    if stack:
        # first breaking open brace
        return False

    return True


t = int(input().strip())
for a0 in range(t):
    expression = input().strip()
    if is_matched(expression) == True:
        print("YES")
    else:
        print("NO")

