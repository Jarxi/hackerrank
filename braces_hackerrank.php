<?php
$handle = fopen ("php://stdin","r");
fscanf($handle,"%d",$t);

for($a0 = 0; $a0 < $t; $a0++){
    fscanf($handle,"%s",$expression);
    $str = str_split($expression);

    $q = new SplStack();

    foreach ($str as $c) {
        if      ($c == '{') $q->push('}');
        else if ($c == '[') $q->push(']');
        else if ($c == '(') $q->push(')');
        else {
            if ($q->isEmpty() || $c != $q[0]) {
                print "NO\n";
                die();
            }

            $q->pop();
        }
    }

    if ($q->isEmpty())
        print "YES\n";
    
    print "NO\n";
}
?>
