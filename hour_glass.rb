#!/bin/ruby

#
# Complete the reverseArray function below.
#
def hour_glass(a)

    indx = 0;
    h = Array.new;

    4.times do |i|
        4.times do |j|
            h.push(a[i][j] + a[i][j+1] + a[i][j+2] + a[i+1][j+1] + a[i+2][j] + a[i+2][j+1] + a[i+2][j+2])
        end
    end

    return h.sort()[h.length-1]
end

arr = Array.new(6)

6.times do |i|
    arr[i] = gets.rstrip.split(' ').map(&:to_i)
end

res = hour_glass arr

puts res
