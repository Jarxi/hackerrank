<?php

class Graph
{
  protected $graph;
  protected $visited = array();

  public function __construct($graph) {
    $this->graph = $graph;
  }

  // find least number of hops (edges) between 2 nodes
  // (vertices)
  public function breadthFirstSearch($origin, $destination) {
    // mark all nodes as unvisited
    foreach ($this->graph as $vertex => $adj) {
      $this->visited[$vertex] = false;
    }

    // create an empty queue
    $q = new SplQueue();

    // enqueue the origin vertex and mark as visited
    $q->enqueue($origin);
    $this->visited[$origin] = true;

    // this is used to track the path back from each node
    $path = array();
    $path[$origin] = new SplDoublyLinkedList();
    $path[$origin]->setIteratorMode(
      SplDoublyLinkedList::IT_MODE_FIFO|SplDoublyLinkedList::IT_MODE_KEEP
    );

    $path[$origin]->push($origin);

    $found = false;
    // while queue is not empty and destination not found
    while (!$q->isEmpty() && $q->bottom() != $destination) {
      $t = $q->dequeue();

      if (!empty($this->graph[$t])) {
        // for each adjacent neighbor
        foreach ($this->graph[$t] as $vertex) {
          if (!$this->visited[$vertex]) {
            // if not yet visited, enqueue vertex and mark
            // as visited
            $q->enqueue($vertex);
            $this->visited[$vertex] = true;
            // add vertex to current path
            $path[$vertex] = clone $path[$t];
            $path[$vertex]->push($vertex);
          }
        }
      }
    }

    if (isset($path[$destination])) {
      echo "$origin to $destination in ", 
        count($path[$destination]) - 1,
        " hopsn";
      $sep = '';
      foreach ($path[$destination] as $vertex) {
        echo $sep, $vertex;
        $sep = '->';
      }
      echo "n";
    }
    else {
      echo "No route from $origin to $destination";
    }
  }
}

function connectedCities($n, $g, $originCities, $destinationCities) 
{
    //$g = 0;
    //$out = array_fill(0, $n, 0);
    $matrix = array(array());

    for ($i = 0; $i < $n; $i++) {
        for ($j = 0; $j < $n; $j++) {
            /*echo gmp_strval(gmp_gcd($i, $j));
            echo $i;
            echo "\n";
            echo $j;*/
		
        $gcd = intval(gmp_strval(gmp_gcd($i, $j)));
        //var_dump($gcd);
	    if ($gcd > $g) {
                $matrix[$i][$j] = 1;
            }
            else {
                $matrix[$i][$j] = 0;
            }
        }
    }
    
    var_dump(sizeof($matrix));
    var_dump(count($originCities));
    var_dump(count($destinationCities));

    $ght = array();
    for ($i = 0; $i < count($originCities); $i++) {
        $ght[] = gmp_strval(gmp_gcd($originCities[$i], $destinationCities[$i]));
    }
    print implode(' ', $ght) . "\n";

    $ght = array();
    print "matrix:\n";
    for ($i = 1; $i <= $n; $i++) {
        for ($j = 1; $j <= $n; $j++) {
            
            if (gmp_strval(gmp_gcd($i, $j)) > 1 && $i != $j) {
                print $i . "," . $j;
                print ": " . gmp_strval(gmp_gcd($i, $j)) . "\n";
            }
        }
    }
    print implode(' ', $ght) . "\n";

    for ($i = 0; $i < count($originCities); $i++) {
        if ($matrix[$originCities[$i]-1][$destinationCities[$i]-1] == 1) {
            $out[] = 1;
        }
        else {
            $out[] = 0;
        }
    }

    return $out;
}

$out = connectedCities(6, 1, [1,2,4,6], [3,3,3,4]);
echo implode(' ', $out);

print "--------------------\n";

$_graph = array();
$_list = array();
$g = 1;$n = 6;
for ($i = 1; $i <= $n; $i++) {
    for ($j = 1; $j <= $n; $j++) {
        if ($i != $j) {
            $gmp = gmp_strval(gmp_gcd($i, $j));
            if ($gmp > $g) {
                $_list[] = $j;
            }
        }
    }
    $_graph[$i] = $_list;
    $_list = array();
}
var_dump($_graph);
print "--------------------\n";

$_g = new Graph($_graph);
$g->breadthFirstSearch(1, 3);
print "--------------------\n";

$graph = array(
    'A' => array('B', 'F'),
    'B' => array('A', 'D', 'E'),
    'C' => array('F'),
    'D' => array('B', 'E'),
    'E' => array('B', 'D', 'F'),
    'F' => array('A', 'E', 'C')
);

$g = new Graph($graph);

// least number of hops between D and C
$g->breadthFirstSearch('D', 'C');
// outputs:
// D to C in 3 hops
// D->E->F->C

// least number of hops between B and F
$g->breadthFirstSearch('B', 'F');
// outputs:
// B to F in 2 hops
// B->A->F

// least number of hops between A and C
$g->breadthFirstSearch('A', 'C');
// outputs:
// A to C in 2 hops
// A->F->C

// least number of hops between A and G
$g->breadthFirstSearch('A', 'G');
// outputs:
// No route from A to G



?>

