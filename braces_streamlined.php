<?php
    //braces_streamlined.php

$handle = fopen ("php://stdin","r");
fscanf($handle,"%d",$t);

function is_balanced($str) {
    
    $q = new SplStack();

    foreach ($str as $key => $c) {
        if ($c == '(' or $c == '[' or $c == '{')
            $q->push($key);
        else if ($c == ')' or $c == ']' or $c == '}') {
            if ($q->isEmpty())
                return 0;
            else
                $q->pop();
        }
        else {
            continue;
        }
    }

    if (!$q->isEmpty()) {
        return $q[count($q) - 1];
    }

    return -1;
}
    
for($a0 = 0; $a0 < $t; $a0++){
    fscanf($handle,"%s",$expression);
    print $expression . "\n";
    $str = str_split($expression);
    
    print is_balanced($str) . "\n";
}

?>