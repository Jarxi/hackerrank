<?php

//$test = array('{[()]}','{[(])}','{{[[(())]]}}','{{[[((s))]]}}','hello world','{}','{{{foo();}}}{}','{{{}','}','{}{foo{}');
//$test = array('{{{}','}','{}{foo{}');
//$test = array('{{{}');
//$test = array('{}{foo{}');

$test = array('}',
    '{',
    '{}{}{}',
    'hello world',
    '{}',
    '{{{foo(;}}}{}',
    '{{}{}}',
    '{{{}',
    '}',
    '{}{foo{}',
    '{}}');

foreach ($test as $t) {
    print $t . "\n";
    print is_balanced($t) . "\n";
}

function is_balanced($str) {
    $str = str_split($str);
    $q = new SplStack();
    $k = 0;

    foreach ($str as $key => $c) {
        if ($c == '{') {
            $q->push($key);
        }
        else if ($c == '}') {
            if (count($q) == 0)
                return $key;
            else
                $q->pop();
        }
        else {
            continue;
        }
    }

    if (count($q) != 0)
        return $q[0];

    return -1;
}
?>
