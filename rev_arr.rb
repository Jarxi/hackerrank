#!/bin/ruby

#
# Complete the reverseArray function below.
#
def reverseArray(a)
    i = 0
    j = a.length - 1

    while i < j do

        last = a[j]
        first = a[i]
          
        a[i] = last
        a[j] = first
      
        i += 1
        j -= 1
      
      end

      return a
      
end

arr_count = gets.to_i

arr = gets.rstrip.split(' ').map(&:to_i)

res = reverseArray arr

puts res.join " "
