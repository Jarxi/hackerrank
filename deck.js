class Deck {
    constructor(n) { 
        this.suit = [
            'Clubs',
            'Hearts',
            'Spades',
            'Diamonds'
        ];

        this.cardNumbers = [
            "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"
        ]

        this.cards = Array();

        this.suit.forEach((s) => {
            this.cardNumbers.forEach((n) => {
                this.cards.push(n + " of " + s);
            });
        });
    }
}

Deck.prototype.shuffle = function() {
    for (var i = 0; i < this.cards.length; i++) {
        let randomCard = Math.floor(Math.random() * 51);
        let c = this.cards[i];
        this.cards[i] = this.cards[randomCard];
        this.cards[randomCard] = this.cards[i];
    }
}

Deck.prototype.pickacard = function() {
    return this.cards[Math.floor(Math.random() * (this.cards.length - 1))];
}
//ES6

Deck.pickacard = () => {
    return cards[Math.floor(Math.random() * (cards.length - 1))];
}


d = new Deck();
console.dir(d.cards);
d.shuffle();
console.dir(d.cards);

while (1) {
    let c = d.pickacard();
    console.log(c);
}


