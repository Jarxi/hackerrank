def get_breaking_index(expression):
    #print (expression)
    stack = []

    for i, c in enumerate(expression):
        if c == '{':
            stack.append(i)
        elif c == '}':
            if not stack:
                # first breaking close brace
                return i
            else:
                stack.pop()
        else:
            # other char
            continue

    if stack:
        # first breaking open brace
        return stack[0]

    return -1


assert get_breaking_index('}') == 0
assert get_breaking_index('{') == 0
assert get_breaking_index('{}{}{}') == -1
assert get_breaking_index('hello world') == -1
assert get_breaking_index('{}') == -1
assert get_breaking_index('{{{foo();}}}{}') == -1
assert get_breaking_index('{{}{}}') == -1
assert get_breaking_index('{{{}') == 0
assert get_breaking_index('}') == 0
assert get_breaking_index('{}{foo{}') == 2
assert get_breaking_index('{}}') == 2